This is a repo to test using GitLab CI to test Ansible playbooks, see the `.gitlab-ci.yml` file and the [Docker files to build the container](https://git.coop/webarch/containers/-/blob/master/ansible/Dockerfile).

References:

* [Testing ansible-roles automatically on every git-push](https://tech.feedyourhead.at/content/testing-ansible-roles-automatically-on-every-git-push)

## Zonefile test 

Test run:

```
ansible-playbook --extra-vars "hostname=localhost" -i "localhost," -c local zonefiles.yml
```

## Hosts file test

Test run:

```
ansible-playbook --extra-vars "hostname=localhost" -i "localhost," -c local etc_hosts.yml
```

This isn't working!

The debug output the following code generates follows:

```
- name: Debug loop through the name servers
  debug:
    msg: "nstld: {{ item }} ns: {{ nstlds[item].nameservers }}"
  with_items: "{{ nstlds | list }}"

```

```
TASK [etchosts : Debug loop through the name servers] ***************************************************************************************************************************************************
ok: [localhost] => (item=webarch.info) => {
    "changed": false, 
    "item": "webarch.info", 
    "msg": "nstld: webarch.info ns: {u'dns3': {u'ipv4addr': [u'93.95.226.180']}, u'dns2': {u'ipv4addr': [u'213.167.241.146']}}"
}
ok: [localhost] => (item=webarchitects.co.uk) => {
    "changed": false, 
    "item": "webarchitects.co.uk", 
    "msg": "nstld: webarchitects.co.uk ns: {u'dns1': {u'ipv4addr': [u'81.95.52.30']}, u'dns0': {u'ipv4addr': [u'81.95.52.24']}}"
}
```

The dictionary structure:

```
  vars:
    nstlds:
      webarchitects.co.uk:
        nameservers:
          dns0:
            ipv4addr:
              - 81.95.52.24
          dns1:
            ipv4addr:
              - 81.95.52.30
      webarch.info:
        nameservers:
          dns2:
            ipv4addr:
              - 213.167.241.146
          dns3:
            ipv4addr:
              - 93.95.226.180
```

An attempt at a template with a double loop to generate /etc/hosts:

```
# output we want is like this:
# 81.95.52.27    dns4.webarchitects.co.uk dns4
{% for tld in nstlds[item].nameservers %}
  {% for ipv4 in tld[item].nameservers %}
{{ ipv4 }}     {{ item }}.{{ tld }} {{ item }}
  {% endfor %}
{% endfor %}
```

